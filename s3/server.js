const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});


app.delete('/deleteProduct',(req,res)=>{
      try{

    for(var i=0;i<products.length;i++){
        if(products[i].productName==req.body.productName){
            products.splice(i,1);
        }
    }
    res.status(200).json(products);
      }
    catch(e){
        res.status(404).json({message : 'not found'});
    }
});

app.put('/updateProduct/:id',(req,res) =>{
    try{
    var product =  products.filter(function(productElement) {
	return productElement.id == req.params.id;
    });
    if(product){
        var index = products.findIndex(prod => prod.id == req.params.id);
        products[index].productName=req.body.productName;
        products[index].price=req.body.price;
        res.status(202).json(product);
    }
    }
    catch(e){
        res.status(404).json({message : 'not found'});
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});