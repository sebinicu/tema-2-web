import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {ProductList} from './ProductList';
import {AddProduct} from './AddProduct';

class App extends Component {
  
    constructor(props){
    super(props);
    this.state={};
    this.state.productList=[];
  }
  
  async componentDidMount(){
    let productList = await this.getAllProducts();
    this.setState({
      productList: productList
    })
    console.log(productList);
  }
  
  getAllProducts = async () => {
    let response = await fetch('https://myworkspace-sebinicu.c9users.io:8081/get-all/');
    let products = await response.json();
    return products;
  }
  updateProducts = (products) => {this.setState({ productList:products })}
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

        <ProductList title="Product List" source={this.state.productList}/>
        <AddProduct source={this.state.productList} updateList={this.updateProducts}/>
          
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
