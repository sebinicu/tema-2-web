import React from 'react'

export class AddProduct extends React.Component{
    

    adaugaProdus(e){
        var productName=document.getElementById("productName");
        var price=document.getElementById("price");
        
     fetch('https://tehnologiiwebnst-ionutnst.c9users.io:8081/add', {
        method: 'POST',
        body: JSON.stringify({"productName":productName.value,"price":price.value}),
        headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    })
    .then(response => response.json())
    //window.location.reload();
    var element=this.props.source;
    element.push({"productName":productName.value,"price":price.value});
    this.props.updateList(element);
    }
    
    
    render(){
           
        return(
            <div>
            <input type="text" id="productName"/><br/>
            <input type="number" id="price"/><br/>
            <button id="btn" onClick={this.adaugaProdus.bind(this)}>ADD PRODUCT</button>
            </div>
            );
    }
    
}